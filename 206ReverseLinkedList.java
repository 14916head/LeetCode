import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

/**
 * Definition for singly-linked list in ListNode.java.
 * 
 * public class ListNode {
 *    int val;
 *    ListNode next;
 *    ListNode() {}
 *    ListNode(int val) { this.val = val; }
 *    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 *
 * [1,2,3,4,5]
 */



class Solution {
    ListNode prev;
    
    public static void main(String[] args) {
        ListNode e = new ListNode(5);
        ListNode d = new ListNode(4, e);
        ListNode c = new ListNode(3, d);
        ListNode b = new ListNode(2, c);
        ListNode a = new ListNode(1, b);

        Solution sol = new Solution();
        ListNode ans = sol.reverseList(a);
        System.out.println(ans.val+" "+ans.next.val+" "+ans.next.next.val+" "+ans.next.next.next.val+" "+ans.next.next.next.next.val);
    }

    public ListNode reverseList(ListNode head) {
        if (head != null && head.next != null) {
            ListNode temp = head.next;
            head.next = prev;
            prev = head;
            return reverseList(temp);
        } else if(head != null) {
            head.next = prev;
            return head;
        } else {
            return head;
        }
    }
}